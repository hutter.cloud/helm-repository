# helm-repository

Deploy a helm repository to AWS S3. Make access possible via AWS Cloudfront

The helm repository can be managed with the [helm-s3 plugin](https://github.com/hypnoglow/helm-s3).
The lambda@edge allows the usage of the repository via cloudfront.

## Requirements

1. AWS Account
2. AWS CLI configured
3. DNS Zone managed in AWS

## Preparation

### terraform baseline

terraform is used to manage the s3 bucket, cloudfront, acm etc.
To use terraform a kms key, s3 bucket, dynamodb and iam user is required

```bash
make prepare
```

### aws resources

next setup your .env file with the access key and id of the terraform user
then execute init, plan and apply

```bash
make init
make plan
make apply
```

### helm registry


After the resources are created the helm registry needs to be initialized
```bash
helm plugin install https://github.com/hypnoglow/helm-s3.git
helm s3 init s3://hutter.cloud-helm-repository/charts/
```

## Using the helm registry

### Adding charts

```bash
# example commands
helm repo add hutter.cloud-s3 s3://hutter.cloud-helm-repository/charts/

helm create test-chart
helm package ./test-chart

HELM_S3_MODE=3 helm s3 push ./test-chart-0.1.0.tgz hutter.cloud-s3
```

### Using the helm registry

The helm registry can be used via https.

```bash
helm repo add hutter.cloud https://helm.hutter.cloud/charts/
helm repo update
helm install hutter.cloud/test-chart
```