#
# terraform definition
#
terraform {
  required_version = "~>0.15"

  backend "s3" {
    bucket         = "hutter.cloud-helm-repository-terraform"
    dynamodb_table = "hutter.cloud-helm-repository-terraform"
    encrypt        = true
    kms_key_id     = "arn:aws:kms:eu-central-1:337261303015:key/1cf0a939-16de-414a-adcf-a6b1a2ab2e94"
    key = "helm-registry.tfstate"
  }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.37.0"
    }
  }
}

#
# providers
#

provider "aws" {}

provider "aws" {
    alias = "us"
    region = "us-east-1"
}

#
# locals
#

locals {
    bucket_name = "hutter.cloud-helm-repository"
    dns_zone = "hutter.cloud"
    host_name = "helm"
    domain_name = "${local.host_name}.${local.dns_zone}"
}

#
# data sources
#

data "aws_route53_zone" "domain_zone" {
  name     = "${local.dns_zone}."
}

#
# s3 bucket
#

resource "aws_s3_bucket" "helm" {
  bucket = local.bucket_name
  acl    = "private"
}

# allow cloudfront to access bucket
resource "aws_s3_bucket_policy" "helm" {
  bucket = aws_s3_bucket.helm.id
  policy = data.aws_iam_policy_document.helm_s3_cloudfront.json
}

data "aws_iam_policy_document" "helm_s3_cloudfront" {
  statement {
    actions   = ["s3:GetObject"]
    resources = ["${aws_s3_bucket.helm.arn}/*"]

    principals {
      type        = "AWS"
      identifiers = [aws_cloudfront_origin_access_identity.helm.iam_arn]
    }
  }
}


# upload index.html
resource "aws_s3_bucket_object" "index" {
  bucket                 = aws_s3_bucket.helm.id
  source                 = "${path.module}/files/index.html"
  key                    = "index.html"
  etag                   = filemd5("${path.module}/files/index.html")
  server_side_encryption = "AES256"
  content_type = "text/html"
}

#
# ci/cd iam user for s3 access
#

resource "aws_iam_user" "helm_user" {
  name = "helmer"
}

data "aws_iam_policy_document" "helm_user_s3_access" {
  statement {
    actions = [
      "s3:ListAllMyBuckets",
      "s3:GetBucketLocation",
    ]
    resources = [
      "arn:aws:s3:::*",
    ]
  }

  statement {
    actions = [
      "s3:ListBucket",
      "s3:*Object",
    ]

    resources = [
      "arn:aws:s3:::${aws_s3_bucket.helm.bucket}",
      "arn:aws:s3:::${aws_s3_bucket.helm.bucket}/*",
    ]
  }
}

resource "aws_iam_user_policy" "helm_user_s3_access" {
  name = "helm_user_s3_access"
  user = aws_iam_user.helm_user.name

  policy = data.aws_iam_policy_document.helm_user_s3_access.json
}

resource "aws_iam_access_key" "helm_user_access_key" {
  user    = aws_iam_user.helm_user.name
}

#
# acm certificate
#

resource "aws_acm_certificate" "certificate" {
  provider          = aws.us
  domain_name       = local.domain_name
  validation_method = "DNS"
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_route53_record" "verification" {
  for_each = {
    for dvo in aws_acm_certificate.certificate.domain_validation_options : dvo.domain_name => {
      name   = dvo.resource_record_name
      record = dvo.resource_record_value
      type   = dvo.resource_record_type
    }
  }

  allow_overwrite = true
  name            = each.value.name
  records         = [each.value.record]
  ttl             = 60
  type            = each.value.type
  zone_id         = data.aws_route53_zone.domain_zone.zone_id
}

resource "aws_acm_certificate_validation" "certificate" {
  provider                = aws.us
  certificate_arn         = aws_acm_certificate.certificate.arn
  validation_record_fqdns = [for record in aws_route53_record.verification : record.fqdn]
}

#
# cloudfront resources
#

resource "aws_cloudfront_origin_access_identity" "helm" {
  comment = "helm cf distribution"
}

resource "aws_cloudfront_distribution" "helm" {
  origin {
    domain_name = aws_s3_bucket.helm.bucket_regional_domain_name
    origin_id   = "helm"
    s3_origin_config {
      origin_access_identity = aws_cloudfront_origin_access_identity.helm.cloudfront_access_identity_path
    }
  }

  enabled             = true
  is_ipv6_enabled     = true
  default_root_object = "index.html"

  aliases = [local.domain_name]

  default_cache_behavior {
    allowed_methods  = ["GET", "HEAD", "OPTIONS"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = "helm"

    forwarded_values {
      query_string = false
      cookies {
        forward = "none"
      }
      headers = []
    }
    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 0
    default_ttl            = 3600
    max_ttl                = 86400
  }

  ordered_cache_behavior {
    path_pattern     = "/charts/index.yaml"
    allowed_methods  = ["GET", "HEAD", "OPTIONS"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = "helm"

    forwarded_values {
      query_string = false
      cookies {
        forward = "none"
      }
    }

    min_ttl                = 0
    default_ttl            = 0
    max_ttl                = 0
    compress               = true
    viewer_protocol_policy = "redirect-to-https"

    lambda_function_association {
      event_type   = "viewer-request"
      lambda_arn   = aws_lambda_function.helm_wrapper.qualified_arn
      include_body = false
    }
  }

  price_class = "PriceClass_100"

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  viewer_certificate {
    cloudfront_default_certificate = false
    acm_certificate_arn            = aws_acm_certificate_validation.certificate.certificate_arn
    ssl_support_method             = "sni-only"
  }
}

# dns record pointing to cloudfront
resource "aws_route53_record" "dns_record" {
  zone_id  = data.aws_route53_zone.domain_zone.zone_id
  name     = local.host_name
  type     = "A"
  alias {
    name                   = aws_cloudfront_distribution.helm.domain_name
    zone_id                = aws_cloudfront_distribution.helm.hosted_zone_id
    evaluate_target_health = true
  }
}


##
# origin request lambda to fix up charts index.yaml urls
##

data "aws_iam_policy_document" "helm_wrapper_assume_role" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com", "edgelambda.amazonaws.com"]
    }
  }
}

data "aws_iam_policy_document" "helm_wrapper_s3_access" {
  statement {
    actions = [
      "s3:ListAllMyBuckets",
      "s3:GetBucketLocation",
    ]
    resources = [
      "arn:aws:s3:::*",
    ]
  }

  statement {
    actions = [
      "s3:ListBucket",
      "s3:GetObject",
    ]

    resources = [
      "arn:aws:s3:::${aws_s3_bucket.helm.bucket}",
      "arn:aws:s3:::${aws_s3_bucket.helm.bucket}/*",
    ]
  }
}

resource "aws_iam_role" "helm_wrapper" {
  name               = "helm_wrapper"
  assume_role_policy = data.aws_iam_policy_document.helm_wrapper_assume_role.json
}

resource "aws_iam_role_policy" "s3_access" {
  name_prefix = "s3access"
  role        = aws_iam_role.helm_wrapper.id
  policy      = data.aws_iam_policy_document.helm_wrapper_s3_access.json
}

resource "aws_iam_policy_attachment" "helm_wrapper_cloudwatch_permissions" {
  name       = "cloudwatch"
  roles      = [aws_iam_role.helm_wrapper.id]
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}


resource "aws_lambda_function" "helm_wrapper" {
  provider         = aws.us
  role             = aws_iam_role.helm_wrapper.arn
  handler          = "lambda.lambda_handler"
  filename         = "${path.module}/../helm-wrapper/helm-wrapper.zip"
  function_name    = "helm-wrapper"
  runtime          = "python3.8"
  source_code_hash = filebase64sha256("${path.module}/../helm-wrapper/helm-wrapper.zip")
  publish          = true
}