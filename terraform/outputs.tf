output "helm_s3" {
  value =aws_s3_bucket.helm.bucket
}

output "helm_url" {
  value = local.domain_name
}

output "helm_ci_user_access_key_id" {
  value = aws_iam_access_key.helm_user_access_key.id
}

output "helm_ci_user_access_key_secret" {
  value = aws_iam_access_key.helm_user_access_key.secret
  sensitive = true
}
