#!/usr/bin/env python3

import os
import boto3

BUCKET_NAME=os.getenv("BUCKET_NAME", default="hutter.cloud-helm-repository")
BUCKET_CHARTS=os.getenv("CHARTS", default="charts")
BUCKET_INDEX_YAML=os.getenv("BUCKET_INDEX_YAML", default="{}/index.yaml".format(BUCKET_CHARTS))


URL_OLD=os.getenv("URL_OLD", default="s3://{}/{}".format(BUCKET_NAME, BUCKET_CHARTS))
URL_NEW=os.getenv("URL_NEW", default="https://helm.hutter.cloud/{}".format(BUCKET_CHARTS))

def get_index_yaml():
    """ download the index.yaml file from s3 """
    try:
        # Download files from S3 Bucket
        s3 = boto3.client('s3')
        object_response = s3.get_object(Bucket=BUCKET_NAME, Key=BUCKET_INDEX_YAML)
        return object_response["Body"].read().decode('utf-8')
    except Exception as e:
        raise e

def lambda_handler(event, context):
    """
        retrieve the index.yaml of the helm chart
        and replace the s3 urls with the https urls
        so the chart repository can be used via cloudfront
    """
    index_yaml = get_index_yaml().replace(URL_OLD,URL_NEW)

    response = {
        'status': '200',
        'statusDescription': 'OK',
        'headers': {
            'content-type': [
                {
                    'key': 'Content-Type',
                    'value': 'text/yaml'
                }
            ]
        },
        'body': index_yaml
    }
    return response

if __name__ == '__main__':
    print(lambda_handler({},{}))