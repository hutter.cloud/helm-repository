.PHONY: prepare



AWS_REGION ?= "eu-central-1"
S3_BUCKET_NAME ?= "hutter.cloud-helm-repository-terraform"
DYNAMODB_NAME ?= "hutter.cloud-helm-repository-terraform"
IAM_USER_NAME ?= "hutter.cloud-helm-repository-terraform"

TERRAFORM_IMAGE ?= "hashicorp/terraform"
TERRAFORM_IMAGE_VERSION ?= "0.15.4"

define TF 
	docker run --rm -v ${PWD}:/data -w /data/terraform \
		--env-file=.env \
		$(TERRAFORM_IMAGE):$(TERRAFORM_IMAGE_VERSION) 
endef 

define LAMBDA
	docker run --rm -v ${PWD}:/data -w /data/helm-wrapper \
		lambci/lambda:build-python3.8
endef

prepare:
ifeq (,$(wildcard ./.kms_key_id))
	# create kms if it doesnt exist yet
	aws --region=$(AWS_REGION) kms create-key --key-usage ENCRYPT_DECRYPT \
  		--origin AWS_KMS \
		--description "Key for terraform tfstate" \
		--query 'KeyMetadata.KeyId' > .kms_key_id
endif

	# setup s3 bucket
	aws --region $(AWS_REGION) s3 mb s3://$(S3_BUCKET_NAME) || true
	
	aws --region $(AWS_REGION) s3api put-bucket-versioning \
		--bucket $(S3_BUCKET_NAME) \
		--versioning-configuration Status=Enabled
	
	aws --region $(AWS_REGION) s3api put-bucket-encryption \
		--bucket $(S3_BUCKET_NAME) \
		--server-side-encryption-configuration \
			"{\"Rules\":[{\"ApplyServerSideEncryptionByDefault\":{\"SSEAlgorithm\":\"aws:kms\",\"KMSMasterKeyID\":$$(cat .kms_key_id)}}]}"
	
	aws --region $(AWS_REGION) s3api put-bucket-tagging \
		--bucket $(S3_BUCKET_NAME) \
		--tagging "TagSet=[]"
	
	aws --region $(AWS_REGION) s3api put-public-access-block \
		--bucket $(S3_BUCKET_NAME) \
		--public-access-block-configuration \
			"BlockPublicAcls=true,IgnorePublicAcls=true,BlockPublicPolicy=true,RestrictPublicBuckets=true"

	# dynamodb
	aws --region $(AWS_REGION) dynamodb create-table \
		--table-name $(DYNAMODB_NAME) \
    	--attribute-definitions "AttributeName=LockID,AttributeType=S" \
    	--key-schema "AttributeName=LockID,KeyType=HASH" \
    	--provisioned-throughput "ReadCapacityUnits=5,WriteCapacityUnits=5" || true

	# iam
	# full admin rights for the terraform user as it needs to manage resources
	aws --region $(AWS_REGION) iam create-user \
		--user-name ${IAM_USER_NAME} || true

	aws --region $(AWS_REGION) iam put-user-policy \
		--user-name $(IAM_USER_NAME) \
		--policy-name "terraform" \
		--policy-document "{\"Version\":\"2012-10-17\",\"Statement\":[{\"Effect\":\"Allow\",\"Action\":\"*\",\"Resource\":\"*\"}]}"

	aws --region $(AWS_REGION) iam create-access-key \
		--user-name $(IAM_USER_NAME) || true


lint:
	$(TF) init -backend=false -input=false
	$(TF) validate

init:
	$(TF) init -input=false

plan:
	$(TF) plan -out="tfplan" -input=false

apply:
	$(TF) apply -input=false "tfplan"

lambda:
	$(LAMBDA) pip3 install --target .package -r requirements.txt
	cd helm-wrapper/.package && zip -r ../helm-wrapper.zip .
	cd helm-wrapper && zip helm-wrapper.zip lambda.py